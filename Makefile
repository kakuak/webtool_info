PY=python3
EXEC=main
SRC=setup.py main.py
BUILD=build/exe.linux-x86_64-3.6
WEBT=webt1.htm webt2.htm
RM=rm -rf

all: $(EXEC)

$(EXEC): $(SRC) $(WEBT)
	$(PY) $< build
	cp $(WEBT) $(BUILD)

.PHONY: clean test

test: $(BUILD)/webt*
	@echo "webt1.htm" | $(BUILD)/main > /dev/null
	@echo "webt2.htm" | $(BUILD)/main > /dev/null
	@cat NAN* | less

clean:
	$(RM) NAN*

rmbuild: build
	$(RM) $<
