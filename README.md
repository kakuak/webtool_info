```
 _       ____________ __________  ____  __       _____   ____________ 
| |     / / ____/ __ )_  __/ __ \/ __ \/ /        /  _/ | / / ____/ __ \
| | /| / / __/ / __  |/ / / / / / / / / /         / //  |/ / /_  / / / /
| |/ |/ / /___/ /_/ // / / /_/ / /_/ / /___     _/ // /|  / __/ / /_/ / 
|__/|__/_____/_____//_/  \____/\____/_____/    /___/_/ |_/_/    \____/  
```

# WEBTOOL INFO

Webtool Info (WI) generate a local file.
All information from Webtool page needed for production is copied into the file.
The local file is named after the purchase order aka "bon de commande (BDC)"

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

Require:
```
Python3
BeautifulSoup
``` 

### Installing

For **Linux** environment, do as...

Install Python 3:
```
sudo apt-get install python3
```
Install BeautifulSoup:
```
sudo pip install bs4
```

### How to use Webtool Info

1.Run the main.py src file

2.Copy the URL of Webtool page in the terminal then press ENTER

3.This generate a file named after the BDC of the purchase order

## Built With

## Authors

* **Rémi Nguyen** - *Initial work* - [Kakuak](https://github.com/kakuak)