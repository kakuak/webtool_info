import sys
from cx_Freeze import setup, Executable

build_exe_options = {"packages": ["bs4", "urllib"], "excludes": [""]}

base = None
if sys.platform == "win32":
    base = "Win32GUI"

setup(  name = "webtool_info",
        version = "0.9.0",
        description = "Extract Webtool Information",
        options = {"build_exe": build_exe_options},
        executables = [Executable("main.py", base=base)])
